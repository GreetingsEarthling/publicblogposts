# Intro

As a Protonmail user, I was interested to learn that Facebook can be configured to send PGP-encrypted emails to its users. I wondered what other services could also provide this service. This note lists the ones I have found.

# List of services that can send PGP encrypted emails

- Facebook
    - [https://protonmail.com/support/knowledge-base/using-protonmail-with-facebook-pgp/](https://protonmail.com/support/knowledge-base/using-protonmail-with-facebook-pgp/)
- Formstack:
    - [https://help.formstack.com/hc/en-us/articles/360019518311-PGP-Email-Encryption](https://help.formstack.com/hc/en-us/articles/360019518311-PGP-Email-Encryption)
- SimpleLogin
    - [https://simplelogin.io/](https://simplelogin.io/)
    > Privacy & Security

    > With PGP, encrypt the forwarded emails with your own PGP key. Only you can then decrypt these emails. Works perfectly with Protonmail or any OpenPGP tools (Enigmail, GPG Tools, etc).

    > To better secure your SimpleLogin account, you can enable 2FA with TOTP and/or WebAuthn (FIDO). 
    - [https://simplelogin.io/faq/](https://simplelogin.io/faq/)
    > Our PGP key for hi@simplelogin.io can be downloaded [here](https://simplelogin.io/hi_at_simplelogin.asc) (fingerprint "BB03 4466 7D70 C5EC 30B5  A07C 704B 2826 4E7C A9E6"). It's also available on keys.openpgp.org

    > If you enable PGP, all emails are signed with signer@simplelogin.io key which can be downloaded [her0e](https://simplelogin.io/signer_at_simplelogin.asc) (fingerprint "7961 6C8F 8E0A 05D1 340F  FC20 4749 AAC8 4D4C 4810"). It's also available on keys.openpgp.org
