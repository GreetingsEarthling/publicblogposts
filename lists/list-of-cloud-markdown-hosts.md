[[_TOC_]]

# List of cloud markdown hosts

## What I'm Looking For
This list is intended to help me track websites that allow you to upload
raw markdown files and will then parse them into HTML (and hopefully style them
too!)

This list will tend towards sites that have some kind of privacy controls, as that is 
a significant requirement for me.

## Detailed Pros and Cons
+ ###### Simplenote by Automattic (Wordpress)
    + Overall impression: Strong but no granular access controls
    + Pros:
        + Completely Free
        + Publish easily, either as plain text or Markdown
        + No version history exposed to public, only latest version
    + Cons:
        + Can share with others but they have to sign up for Simplenote, which doesn't have social (except for Wordpress.com)
            + Collaborators can edit, not just read
        + No built in file storage for pictures, videos
+ ###### Github repositories (see any readme.md)
    + Overall impression: The gold standard
    + Pros:
        + Excellent styling
        + Privacy controls
        + Also supports ReStructured Text, if you're into that
    + Cons:
        + Requires making version history available.
            + This makes "unpublishing" something you published by mistake very hard
        + Desktop sync requires familiarity with Git (not easy for non-coders)
        + Privacy controls for more than a few viewers require paid plan
+ ##### Github gists / Cacher
    + Pros
        + Simple reading/writing interface for articles intended to be public
        + Supports ReStructured Text, if you're into that
    + Cons
        + Cannot truly make a gist/cache private. It must be "unlisted" and can be discovered/leaked if URL accidentally gets published
        + Images must be hosted elsewhere
+ ##### Gitlab
    + Pros
        + Excellent styling
        + Privacy controls
        + Also supports ReStructured Text, if you're into that
        + Free for unlimited number of private subscribers
    + Cons
        + too much unnecessary UI cruft. On some smart devices, content isn't even visible in the initial viewport
        + Requires making version history available
            + This makes "unpublishing" something you published by mistake very hard
        + Desktop sync requires familiarity with Git (not easy for non-coders)
+ ##### git.sr.ht (from Sourcehut)
    + Pros
        + Attactive styling
        + Not too much UI cruft
        + Free (for now)
    + Cons
        + Requires making version history available
        + No web update interface at all (web interface is read only)
        + Plans to become paid-only after Alpha is done
+ ##### Keybase.pub (e.g. [this site](https://chris.keybase.pub/))
    + Pros
        + Absolutely no UI cruft
        + Desktop sync built in
    + Cons
        + Absolutely zero default styling (requires you to upload or embed your own stylesheet) 
            + Check out [this page](https://davidsmedberg.keybase.pub/new-post) for an example of a somewhat-Github-styled page
                + [This](https://keybase.pub/davidsmedberg/new-post/index.md) is the raw markdown plus HTML to link to the stylesheet
            + But the [homepage](https://davidsmedberg.keybase.pub/) doesn't even have any header.
        + Private file stores do not output to an HTML site, only public file stores
        + BUG: Relative links are relative to the document root, not to the location of the markdown file
            + e.g. a markdown file in test/index.md that includes image.png will look for /image.png, not /test/image.png
+ #### LBRY.tv
    + Pros
        + Good stylesheet / formatting
        + Opportunity to request tips (if you are comfortable with a blockchain coin)
        + Opportunity to charge for viewing the file
        + Can publish anonymously
        + Can be discovered as part of the social media on the site
    + Cons
        + No private publishing - only public
        + Images must be hosted elsewhere
        + Markdown support not documented and no preview available before publishing
