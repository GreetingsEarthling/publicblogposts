# Mini-review: Thomas Bergersen's "Sun"

![Album cover for Thomas Bergersen's album "Seven"](image.jpg)

I first became aware of Thomas Bergersen via his role in the duo ["Two Steps from Hell"](https://www.twostepsfromhell.com/). Two Steps from Hell makes dramatic orchestral/choral music for trailers, some of which is even worth listening to on its own! Honestly, however, I came to view Nick Phoenix's compositions as decidedly less awesome than Bergersen's, and I increasingly sought out Bergersen's solo work. His album ["Sun"](https://www.amazon.com/Sun-Thomas-Bergersen/dp/B00MXBIPQK/) had a high point in the track "Final Frontier", which has been on my most-played rotation (and not just mine - it made it into [a trailer](https://www.youtube.com/watch?v=0vxOhd4qlnA) for the movie "Interstellar").

Sun is pretty much the Platonic ideal of music marketed to trailers. It is gorgeous and inspiring, but limited in its emotional range because it has to be able to be edited into discrete sound bites of easily-digestible dramatic pablum. Bergersen has recently been trying to move past the limits of the trailer music genre. He has written 2 albums which are less easily sliced and diced, but carry themes and musical narrative through-lines that, to me, make for more interesting repeated listening. 

The first, ["American Dream"](https://www.amazon.com/American-Dream-Thomas-Bergersen/dp/B07NH7R4YZ/) is themed after American greats like Aaron Copland, so it didn't feel very distinctive to Bergersen. Don’t get me wrong – it’s still optimistic, rousing and worth listening to. But it’s only with ["Seven"](https://www.amazon.com/dp/B07QX9GMHX/) that I feel that Bergersen has really come into his own.

"Seven" tells the abstract story of one person’s life, from its hopeful beginning to peaceful end, with some triumph and some heartbreak in the middle. Like the best of Bergersen, it’s imbued with optimism and charisma. Maybe it’s because I just had a newborn, but it connects with my hopes and dreams for her on a deep level.

If you like orchestral movie scores, or classical music, give it a listen!