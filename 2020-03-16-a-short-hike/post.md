# Recommended PC Game: "A Short Hike"

Quick, you have until some time on Thursday. For the low, low price of $0, you can pick up an absolutely splendid, family-friendly PC game called "A Short Hike" over at the [Epic Games Store](https://www.epicgames.com/store/en-US/product/a-short-hike/home). 

As I:
+ Grow older;
+ Play more video games with my kids;

...I've come to appreciate more and more the games that take you by the hand, not to tell you exactly where to go, but to encourage you to explore and discover. 

Many games *allow* you to explore, but not as many *guide* you in how to explore. It's entirely apropos that the game begins with a friendly park ranger and is anchored by a visitor center. It's also worth noting that "massively open world" is often inferior to "intimate open world". In a game such as "A Short Hike", care was given to each and every character's dialogue. The crusty old ship's captain who's afraid you'll snitch on him for taking a break. The *hilarious* banter of the children who teach you to play beach ball. The high-strung mountain climber who turns out to need a helping hand. Each of these and more demonstrate the care and attention given to this little island paradise.

The magical moment, for me, was when I reached a tall cliff, realized I didn't want to keep climbing, and simply opened my wings (you play as a talking bird) and glided off. I soared with the wind currents and gradually the haze below coalesced into land I'd never even realized existed. A wonderful sense of freedom filled me and the day's pressures were washed off.

My 5-year-old loves to watch me play games but can't handle a controller. So I told him, "I'm going to be holding the controller, but you tell me what to do - you're in charge." And he immediately got it - it's simple enough for a child. I officially count "A Short Hike" as his first game he actually played. He felt so connected to a game world that's recognizably similar to his reality, but also felt empowered by his ability to experiment without consequences of failure (perhaps one of the best things about video games). When he realized he had caught a rare albino trout and sold it for 80c to the captain, he just about popped with pride 👌, especially since I didn't catch it for him - I made him tell me where to cast the line and when to reel it in.

It works for Mac and Linux as well as Windows, by the way. You've really got no excuse at this point. 🙂 It's also available from [Steam](https://store.steampowered.com/app/1055540/A_Short_Hike/) and [Itch](https://adamgryu.itch.io/a-short-hike), in case you have a preferred storefront. Check out the reviews on Steam if you're still on the fence - it's "Overwhelmingly Positive".
<!-- tags: gaming, family -->