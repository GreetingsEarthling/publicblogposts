# 2020 MLB Bracket

Ever since the Nationals pulled me back into baseball fandom, I've enjoyed
creating a bracket every year. I'm not especially skilled or anything, so take
this with all the necessary grains of salt.

I almost feel weird predicting that the Nationals will lose in the NLCS, since
it in the past we've either lost in the NLDS or gone on to win it all. But 
there's a time and a place for everything! 

Anyone keeping an eye on this postseason can tell that the Dodgers and the 
Yankees are both highly motivated to go the distance this year, and as much as 
I'd love to see someone unseat them from their ambitions, I can't realistically
predict that.

In addition to my gut feelings, I used comparisons from:
+ [USA Today's panel of experts](http://web.archive.org/web/20200215005947/https://www.usatoday.com/story/sports/mlb/2020/02/04/mlb-2020-baseball-predictions-win-totals/4649827002/)
+ [Fangraphs' win projections as of 2020/2/11](https://www.fangraphs.com/depthcharts.aspx?position=Standings)
+ The DraftKings and FanDuels [World Series odds](https://www.thelines.com/odds/world-series/) (again as of 2020/2/11)

Update 2020/2/16: Baseball Prospectus has published [their projections](https://www.baseballprospectus.com/standings/)
based on the PECOTA algorithm. Still waiting for FiveThirtyEight to publish their 
projections as they have in the past.

![My 2020 MLB predictions.](bracket.png)
