# How to use this blog

You may have noticed that all posts on this blog only contain a short preview, with a link to read more. Usually, that link is public, so anyone can read the post, but once in a while you'll see that the "Read More" link is labeled as private. If so, clicking the private link will only show an error page. As of the day I'm writing this guide, that error message will look something like this:

![Gitlab 404 Error Message](error.PNG "Gitlab 404 Error Message")

The reasons that I handle private posts this way are something I'll write a post about another time. But in this post, I'm writing to help you get access to the private links. If we've met in real life, and you'd like to be able to keep up with my family and other private thoughts, I'd love to have you!

You'll only need one thing: You'll need to be comfortable using a Google or Twitter login to set up a free account with Gitlab, a company that's usually used by IT professionals to track code. That's the company I use to host my blog posts. (Exactly why I use them will have to be a topic for another post.) If you don't have a Google or Twitter account, you can still create a free account just for Gitlab.

Getting access for the first time will require that you follow 3 simple steps.
1. If you don't already have a Gitlab account, visit [the signup page](https://gitlab.com/users/sign_in#register-pane) to create a new free account. As mentioned before, you can use a Google or Twitter account to log in so you don't have to remember another password.
2. Once you have an account, visit [this group page](https://gitlab.com/david-smedbergs-friends-and-family). It should say "David Smedberg's Friends and Family". There should be a link there that says "Request Access". (See the image below.) Click the "Request Access" link.

![A screenshot of the "Request Access" button](snip.PNG "A screenshot of the 'Request Access' button")

3. Wait to receive an email confirming that I've granted you access. The email should say "Access to the David Smedberg's Friends and Family group was granted".

That's it! Now that you're ready, you can return to the private blog post and click the link again. You'll be able to view the post without any further difficulty! If you'd like to test your access, please [click here](https://gitlab.com/GreetingsEarthling/privateblogposts/blob/master/test/post.md) to view a test blog post.

<!--  tags: Technology, tech tips -->