# Is Seattle caught in the "2-income trap"?

Recently, Seattle has been coping with the threat of a mass outbreak of the 
coronavirus. Their public health officials have begun issuing encouragements to
work from home and avoid large gatherings. Nevertheless as [Tim Carney points out](https://www.washingtonexaminer.com/opinion/stay-at-home-parents-could-keep-us-safer-amid-coronavirus-outbreak),
the public school authorities have declined to close the schools. Carney wonders
whether families with a stay-at-home parent would be able to cope with extreme 
events such as coronavirus better than those where both parents work.

Carney's political alignment is with the right, but his point is not partisan.
Elizabeth Warren's 2003 book ["The Two-Income Trap"](https://www.goodreads.com/book/show/854759.The_Two_Income_Trap)
discusses the ways that households
(with and without children) can be paradoxically stronger by doing less - because
this gives them more flexibility to "ramp up" and do more during times of crisis. 
If the members of a household are already working as hard/earning as much as they can, 
then when times get rough they don't have any practical way to respond to their
new challenges.

Just as Warren's book was met with much criticism, similar objections can be made to
Carney's point. There is no one-size-fits-all recipe for supporting or raising a 
family. A household's decision might make everyone better off, but 
disadvantage that household itself (a [collective action problem](https://en.wikipedia.org/wiki/Collective_action_problem)). 
And perhaps most importantly, there are ideals which one can praise, but can be in
practice make starting a family seem unattainable, leading to a lower rate of family
formation exactly in times when we hope to encourage it.