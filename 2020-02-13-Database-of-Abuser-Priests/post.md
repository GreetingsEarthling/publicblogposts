# Database of Abuser Priests

ProPublica has done all of us a great favor. 

If you have been following the abuse crisis in the Catholic Church, you know that many dioceses and religious orders have (either voluntarily or otherwise) released reports including each credibly accused priest. However, it has always been very nearly impossible to compare these lists (or even, in some cases, to find them on the diocesan/order website).

Now, ProPublica has gone above and beyond, entering all the reports into a [searchable database](https://projects.propublica.org/credibly-accused/). This database also includes a dashboard that shows which reports are still missing or unreleased. 

![The first few entries on the dashboard.](dashboard.png)

If you click on the name of the diocese, you can also:

+ View a list of all the priests from the diocese that were included
+ Click "Published List" to see the report in its original form as reported from the diocese, including any additional information that diocese may have included (as there is a great deal of variety in what each diocese reports).

You can also search for individual names. For example, if you search for "McCarrick" you'll find that the disgraced Cardinal [has been included in reports from 4 dioceses](https://projects.propublica.org/credibly-accused/search/?utf8=%E2%9C%93&q=mccarrick&commit=Search).

![A screenshot of the search results.](searchresult.png)

The database is only as good as the reports it comes from, so remember not to blame ProPublica if information is missing or incorrect.

<!-- tags: religion, sexual abuse crisis, Catholicism -->