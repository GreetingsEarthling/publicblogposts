# How to get an email each time a new blog post is published

If a friend publishes a new post on Facebook, you can set up your settings in such a way that you receive a notification. But what about blogs? Is it possible to get a notification each time a new blog post is published? Because blogging uses an system called RSS (Really Simple Syndication), there are many ways. Let me share one of the simplest, from a service called Blogtrottr. Let's use [my blog](https://davidsmedberg.weebly.com) as an example!

**Steps:**
- Visit [blogtrottr.com](https://www.blogtrottr.com)
- Under "Getting Started", enter the following feed address
    https://davidsmedberg.weebly.com/1/feed
- Enter your email address, and press "Feed Me"!
- Confirm your email address by clicking the link in the email you receive

That's it! Now you'll get a notification email each time a new post is published.

Another example of a similar service is [IFTTT](https://ifttt.com/) (If This Then That). 

(Note to friends: If this seems a bit overwhelming, feel free to [email me](https://davidsmedberg.weebly.com/contact.html) and I can set it up for you.)

If you'd like to keep up with a number of blogs by browsing a list of new posts, a great service I'd recommend is [NewsBlur](https://www.newsblur.com). Each of these services is free for users who only want to follow a few blogs, and they're quite cheap even if you start to follow many blogs at once.

<!-- tags: Technology, tech tips -->